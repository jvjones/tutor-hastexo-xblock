import io
import os
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))

with io.open(os.path.join(here, "README.rst"), "rt", encoding="utf8") as f:
    readme = f.read()

about = {}
with io.open(
    os.path.join(here, "tutorhastexo", "__about__.py"),
    "rt",
    encoding="utf-8",
) as f:
    exec(f.read(), about)

setup(
    name="tutor-hastexo-xblock",
    use_scm_version={
        "root": "..",
        "relative_to": __file__,
        "local_scheme": "node-and-timestamp"
    },
    url="https://bitbucket.org/jvjones/tutor-hastexo-xblock/",
    project_urls={
        "Code": "https://bitbucket.org/jvjones/tutor-hastexo-xblock/",
        "Issue tracker": "https://bitbucket.org/jvjones/tutor-hastexo-xblock/issues",
    },
    license="AGPL-3.0",
    author='Joseph Jones',
    description="An updated Tutor plugin for the hastexo XBlock",
    long_description=readme,
    packages=find_packages(exclude=["tests*"]),
    include_package_data=True,
    python_requires=">=3.5",
    install_requires=[
        "setuptools-scm",
        "tutor-openedx",
    ],
    entry_points={
        "tutor.plugin.v0": [
            "hastexo = tutorhastexo.plugin"
        ]
    },
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Education",
        "License :: OSI Approved :: GNU Affero General Public License v3",
        "Operating System :: POSIX :: Linux",
        "Topic :: Education :: Computer Aided Instruction (CAI)",
        "Topic :: Education",
    ],
    setup_requires=["setuptools-scm"],
)
